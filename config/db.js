var knex = require('knex')({
	client: 'mysql',
	connection: {
		host: 'localhost',
		user: 'bookshelfTest',
		password: 'bookshelf',
		database: 'bookshelfTest'
	}
}),
bookshelf = require('bookshelf')(knex);

module.exports.bookshelf = bookshelf;
