var User = require('../models/user.js'),
crypt = require('../helpers/crypt.js');

module.exports = {
	fetchUserSensitive: function(userProperties, callback) { // includes password hash
		var options = {};
		module.exports.fetchUser(userProperties, options, callback);
	},
	fetchUserProfile: function(userProperties, callback) {
		var options = {
			withRelated: ['avatar'],
			columns: ['user_id', 'username']
		};
		module.exports.fetchUser(userProperties, options, callback);
	},
	fetchUser: function(userProperties, options, callback) { // genericized
		var fetchedUser = new User.model({});
		fetchedUser.set(userProperties);
		
		fetchedUser.fetch(options).then(function(model) {
			if (!model) {
				callback(new Error('User does not exist'), null);
			}
			else {
				callback(null, model);
			}
		});
	},
	createUser: function(username, password, callback) {
		var defaultAvatarId = 1,
		createdUser;
		crypt.hash(password, function(hashErr, hashedPassword) {
			if (hashErr) {
				callback(hashErr, null);
			}
			else {
				createdUser = new User.model({
					username: username,
					password: hashedPassword,
					avatar_id: defaultAvatarId
				});
				createdUser.save().then(function() {
					callback(null, createdUser);
				});
			}
		});
	}
}
