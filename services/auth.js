var crypt = require('../helpers/crypt.js'),
userService = require('../services/users.js');

module.exports = {
	checkAuth : function(username, rawPassword, callback) {
		var hashedPassword;
		userService.fetchUserSensitive({username: username}, function(fetchErr, fetchedUser) {
			if (fetchErr) {
				callback(fetchErr, null);
			}
			else {
				hashedPassword = fetchedUser.get('password');
				crypt.compare(rawPassword, hashedPassword, function(compareErr, compareResult) {
					if (compareResult) {
						callback(compareErr, fetchedUser);
					}
					else {
						callback(compareErr, null);
					}
				});
			}
		});
	}
}
