var Post = require('../models/post.js');

module.exports = {
	fetchAllPosts: function(callback) {
		var fetchedPosts = new Post.collection({}),
		options = {
			withRelated: ['user.avatar']
		};

		fetchedPosts.query('orderBy', 'post_id', 'desc').fetch(options).then(function(collection) {
			collection.removeSensitive();
			callback(null, collection);
		});
	},
	fetchUserPosts: function(userId, callback) {
		var fetchedPosts = new Post.collection({}),
		options = {
			withRelated: ['user.avatar']
		};

		fetchedPosts.query('where', 'user_id', '=', userId).query('orderBy', 'post_id', 'desc').fetch(options).then(function(collection) {
			collection.removeSensitive();
			callback(null, collection);
		});
	},
	createPost: function(title, body, userId, callback) {
		createdPost = new Post.model({
			title: title,
			body: body,
			user_id: userId
		});
		// date is a default on the model
		createdPost.save().then(function() {
			callback(null, createdPost);
		});
	}
}
