var reqUtils = require('../helpers/req.js');

module.exports = {
	requireLogin: function(req, res, next) {
		var userId = req.session.userId;
		if (!userId) {
			reqUtils.setError(req, 'You must be logged in to access that.');
			res.redirect('/');
		}
		else {
			next();
		}
	}
}
