var express = require('express'),
reqUtils = require('../helpers/req.js'),
authMiddleware = require('../middleware/auth.js'),
PostController = require('../controllers/posts.js'),
UserController = require('../controllers/users.js'),
router = express.Router(),
routerPrivate = {};


router.get('/', function(req, res, next) {
	var viewData = reqUtils.setDefaultViewData(req),
	controller = new PostController();

	viewData.pageTitle = 'Home';
	
	controller.getAllPosts(req, function(err, result) {
		viewData.posts = result.toJSON();
		res.render('index', viewData);
		reqUtils.clearAll(req);
	});
});

router.get('/login', function(req, res, next) {
	var viewData = reqUtils.setDefaultViewData(req);
	viewData.pageTitle = 'Login';
	res.render('login', viewData);
	reqUtils.clearAll(req);
});

router.get('/register', function(req, res, next) {
	var viewData = reqUtils.setDefaultViewData(req);
	viewData.pageTitle = 'Register';
	res.render('register', viewData);
	reqUtils.clearAll(req);
});

router.get('/posts/write', authMiddleware.requireLogin);
router.get('/posts/write', function(req, res, next) {
	var viewData = reqUtils.setDefaultViewData(req);
	viewData.pageTitle = 'Write post';
	res.render('writePost', viewData);
	reqUtils.clearAll(req);
});

router.get('/users/:userId', function(req, res, next) {
	var viewData = reqUtils.setDefaultViewData(req),
	controller = new UserController();
	
	controller.getProfile(req, function(err, result) {
		viewData.user = result.toJSON();
		viewData.pageTitle = viewData.user.username + '\'s profile';
		res.render('profile', viewData);
		reqUtils.clearAll(req);
	});
});

router.get('/users/:userId/posts', function(req, res, next) {
	var viewData = reqUtils.setDefaultViewData(req),
	controller = new PostController();

	controller.getUserPosts(req, function(err, result) {
		viewData.posts = result.toJSON();
		viewData.pageTitle = viewData.posts[0].user.username + '\'s posts';
		res.render('posts', viewData);
		reqUtils.clearAll(req);
	});
});

module.exports = router;
