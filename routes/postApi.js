var express = require('express'),
PostController = require('../controllers/posts.js'),
reqUtils = require('../helpers/req.js'),
authMiddleware = require('../middleware/auth.js'),
router = express.Router();

router.post('/', authMiddleware.requireLogin);
router.post('/', function(req, res, next) {
	var controller = new PostController();
	console.log('creating new post');
	controller.createPost(req, function(err, result) {
		if (err) {
			reqUtils.setError(req, 'Failed to create post');
			res.redirect('/posts/write');
		}
		else {
			reqUtils.setMessage(req, 'Post created successfully');
			res.redirect('/');
		}
	});
});

module.exports = router;
