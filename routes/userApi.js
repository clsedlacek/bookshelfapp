var express = require('express'),
UserController = require('../controllers/users.js'),
reqUtils = require('../helpers/req.js'),
router = express.Router();

router.post('/', function(req, res, next) {
	var controller = new UserController();
	console.log('registering new user '+req.body.username+'...');
	controller.register(req, function(err, result) {
		if (err) {
			reqUtils.setError(req, 'A user with this name already exists.');
			res.redirect('/register');
		}
		else {
			reqUtils.setMessage(req, 'Registered successfuly as '+req.body.username+', you may now log in.');
			res.redirect('/');
		}
	});
});

module.exports = router;
