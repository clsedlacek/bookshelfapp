var express = require('express'),
LoginController = require('../controllers/login.js'),
reqUtils = require('../helpers/req.js'),
router = express.Router();

router.post('/', function(req, res, next) {
	var controller = new LoginController();
	controller.login(req, function(err, result) {
		if (err || !result) {
			reqUtils.setError(req, 'Incorrect username or password.');
			res.redirect('/login');
		}
		else {
			res.redirect('/');
		}
	});
});

router.all('/logout', function(req, res, next) {
	var controller = new LoginController();
	controller.logout(req, function(err, result) {
		res.redirect('/');
	});
});

module.exports = router;
