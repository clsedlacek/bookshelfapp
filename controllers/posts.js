var postService = require('../services/posts.js');

function PostController() {
}
PostController.prototype.createPost = function(req, callback) {
	var postTitle = req.body.postTitle,
	postBody = req.body.postBody,
	userId = req.session.userId;

	postService.createPost(postTitle, postBody, userId, function(err, result) {
		callback(err, result);
	});
}
PostController.prototype.getAllPosts = function(req, callback) {
	postService.fetchAllPosts(function(err, result) {
		callback(err, result);
	});
}
PostController.prototype.getUserPosts = function(req, callback) {
	var userId = req.params.userId;
	
	postService.fetchUserPosts(userId, function(err, result) {
		callback(err, result);
	});
}

module.exports = PostController;
