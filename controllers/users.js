var userService = require('../services/users.js');

function UserController() {
}
UserController.prototype.register = function(req, callback) {
	var username = req.body.username,
	password = req.body.password;
	userService.fetchUserSensitive({username: username}, function(err, userExists) { // check if username taken
		if (userExists) {
			callback(new Error('A user with this name already exists.'), null);
		}
		else {
			userService.createUser(username, password, function(err, createdUser) {
				callback(err, createdUser);
			});
		}
	});
}
UserController.prototype.getProfile = function(req, callback) {
	var userId = req.params.userId;
	userService.fetchUserProfile({user_id: userId}, function(err, result) {
		callback(err, result);
	});
}

module.exports = UserController;
