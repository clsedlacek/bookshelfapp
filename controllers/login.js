var authService = require('../services/auth.js');

function LoginController() {
}
LoginController.prototype.setSessionUser = function(req, user) {
	var username = user.get('username'),
	userId = user.get('user_id');
	
	if (!username || !userId) {
		throw new Error('setSessionUser: missing username or userId');
	}
	req.session.username = username;
	req.session.userId = userId;
	console.log('session info set');
}
LoginController.prototype.login = function(req, callback) {
	var username = req.body.username,
	rawPassword = req.body.password,
	loginSuccess = false,
	self = this;
	
	authService.checkAuth(username, rawPassword, function(err, loggedInUser) {
		if (loggedInUser) {
			self.setSessionUser(req, loggedInUser);
			loginSuccess = true;
		}
		callback(err, loginSuccess);
	});
}
LoginController.prototype.logout = function(req, callback) {
	var logoutSuccess = true;
	req.session.destroy(function(err) {
		if (err) {
			console.log('logout err: '+err);
			logoutSuccess = false;
		}
		callback(err, logoutSuccess);
	});
}

module.exports = LoginController;
