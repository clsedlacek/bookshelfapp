// express setup
var express = require('express'),
app = express(),
port = 3000,
cookieParser = require('cookie-parser'),
bodyParser = require('body-parser'),
session = require('express-session'),
flash = require('connect-flash');

app.set('view engine', 'ejs');
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(session({secret: 'ICameToBringThePain'}));
app.use(flash());

app.use('/', require('./routes/main.js'));
app.use('/api/login', require('./routes/loginApi.js'));
app.use('/api/users', require('./routes/userApi.js'));
app.use('/api/posts', require('./routes/postApi.js'));
app.use(express.static(__dirname + '/public'));

app.listen(port);
console.log('App listening on port '+port);
