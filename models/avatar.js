var bookshelf = require('../config/db.js').bookshelf;

var AvatarModel = bookshelf.Model.extend({
	tableName: 'avatars',
	idAttribute: 'avatar_id'
});

var AvatarCollection = bookshelf.Collection.extend({
	model: AvatarModel
});

module.exports = {
	model: AvatarModel,
	collection: AvatarCollection
};
