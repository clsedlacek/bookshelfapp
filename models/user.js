var bookshelf = require('../config/db.js').bookshelf,
Avatar = require('./avatar.js'),
Post = require('./post.js');

var UserModel = bookshelf.Model.extend({
	tableName: 'users',
	idAttribute: 'user_id',
	avatar: function() {
		return this.hasOne(Avatar.model, 'avatar_id');
	},
	post: function() {
		return this.hasMany(Post.model, 'user_id');
	},
	removeSensitive: function() {
		this.unset('password');
	}
});

module.exports = {
	model: UserModel
};
