var bookshelf = require('../config/db.js').bookshelf,
User = require('./user.js'),
dateUtils = require('../helpers/date.js');

var PostModel = bookshelf.Model.extend({
	tableName: 'posts',
	idAttribute: 'post_id',
	defaults: {
		posted_date: dateUtils.toMysqlDateTime(new Date())
	},
	user: function() {
		return this.belongsTo(User.model, 'user_id');
	}
});

var PostCollection = bookshelf.Collection.extend({
	model: PostModel,
	removeSensitive: function() {
		this.map(function(model) {
			var user = model.related('user');
			if (user) {
				user.removeSensitive();
			}
		});
	}
});

module.exports = {
	model: PostModel,
	collection: PostCollection
};
