module.exports = {
	toMysqlDateTime: function(dateObj) {
		return dateObj.toISOString().slice(0, 19).replace('T', ' ');
	}
}
