module.exports = {
	setDefaultViewData : function(req) {
		var viewData = {
			error: req.flash('error') || null,
			message: req.flash('message') || null,
			username: req.session.username || null,
			userId: req.session.userId || null
		};

		return viewData;
	},
	clearError : function(req) {
		req.flash('error', null);
	},
	clearMessage : function(req) {
		req.flash('message', null);
	},
	clearAll : function(req) {
		module.exports.clearError(req);
		module.exports.clearMessage(req);
	},
	setError: function(req, err) {
		req.flash('error', err);
	},
	setMessage: function(req, message) {
		req.flash('message', message);
	}
};
