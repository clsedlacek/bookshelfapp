var bcrypt = require('bcrypt-nodejs');

module.exports = {
	hash: function(data, callback) {
		bcrypt.hash(data, null, null, function(err, hash) {
			callback(err, hash);
		});
	},
	compare: function(rawData, hashedData, callback) {
		bcrypt.compare(rawData, hashedData, function(err, res) {
			callback(err, res);
		});
	}
}

